/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.classes;

/**
 *
 * @author Horacio Zerpa Diaz <hzerpad@uni.pe>
 */
public class TestStackOfIntegers {
    public static void main(String[] args) {
        StackOfIntegers stack = new StackOfIntegers();
        for(int i = 0; i < 20; i++){
            stack.push(i);
        }
        
        while(!stack.empty()){
            System.out.println(stack.pop() + " ");
        }
    }
}
