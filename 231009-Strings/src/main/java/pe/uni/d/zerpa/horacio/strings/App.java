/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.uni.d.zerpa.horacio.strings;

/**
 *
 * @author Horacio
 */
public class App {

    public static void main(String[] args) {
        String string = "No quiero jalar";
        System.out.println(string);

        //Length
        System.out.println("length of string: " + string.length());

        //character location
        System.out.println(string.charAt(0));
        System.out.println(string.charAt(string.length() - 1));

        //convert
        System.out.println(string.toUpperCase());

        //split
        String[] partes = string.split(" ");
        for (int i = 0; i < partes.length; i++) {
            System.out.println(partes[i]);
        }
        
        //trim
        String trim = string.trim();
        System.out.println("trim: " + trim);
        System.out.println("length of trim: " + trim.length());
    }
}
