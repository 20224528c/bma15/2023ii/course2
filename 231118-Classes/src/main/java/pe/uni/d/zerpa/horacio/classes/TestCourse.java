/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.uni.d.zerpa.horacio.classes;

/**
 *
 * @author Horacio Zerpa Diaz <hzerpad@uni.pe>
 */
public class TestCourse {

    public static void main(String[] args) {
        System.out.println("Test Course!");
        String[] cursos = {"Estructura de datos", "Sistema de datos", "Programación orientado a objetos"};
        String[] alumnos = {"Horacio Zerpa", "José Fernandez", "Raúl Villanueva"};

        //Matrícula inicialización
        Course course1 = new Course(cursos[0]);
        Course course2 = new Course(cursos[0]);
        Course course3 = new Course(cursos[0]);

        //Matrícula carga de alumnos
        course1.addStudent(alumnos[0]);
        course1.addStudent(alumnos[2]);

        course2.addStudent(alumnos[1]);
        course2.addStudent(alumnos[2]);

        course3.addStudent(alumnos[0]);
        course3.addStudent(alumnos[1]);

        //Matrícula reporte
        System.out.println("Número  de los alumnos en el curso " + course1.getCourseName() + ":" + course1.getNumberOfstudents());
        viewsStudents(course1);
        System.out.println("Número  de los alumnos en el curso " + course2.getCourseName() + ":" + course2.getNumberOfstudents());
        viewsStudents(course2);
        System.out.println("Número  de los alumnos en el curso " + course3.getCourseName() + ":" + course3.getNumberOfstudents());
        viewsStudents(course3);
        
    }

    private static void viewsStudents(Course course) {
        String[] alumnos = course.getStudents();
        
        for(int i = 0; i < course.getNumberOfstudents(); i++){
            System.out.print(alumnos[i] + ",");
        }
        System.out.println("");
    }
}
