/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.collections;

import java.util.Comparator;

/**
 *
 * @author Horacio Zerpa Diaz <hzerpad@uni.pe>
 */
public class TestComparator {

    public static void main(String[] args) {
        System.out.println("Test Comparator");
        GeometricObject go1 = new Rectangle(5, 5);
        GeometricObject go2 = new Circle(5);

        //System.out.println("Comparacion: " + comparar(go1,go2));
        GeometricObject go = mayor(go1, go2, new GeometricObjectComparator());
        System.out.println("El area del objeto mas grande: " + go.getArea());
    }

    private static GeometricObject mayor(GeometricObject o1, GeometricObject o2, Comparator<GeometricObject> c) {
        if (c.compare(o1, o2) >= 0) {
            return o1;
        } else {
            return o2;
        }
    }
    /*
    private static int comparar(GeometricObject o1, GeometricObject o2) {
        double area1 = o1.getArea();
        double area2 = o2.getArea();
        if (area1 == area2) {
            return 0;
        } else if (area1 > area2) {
            return 1;
        } else {
            return 2;
        }
    }
     */

}
