/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.classes;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public class Circle {

    double radious;

    Circle() {
        this.radious = 1;
    }

    Circle(double newRadious) {
        this.radious = newRadious;
    }

    double getArea() {
        return Math.PI * this.radious * this.radious;
    }

    double getPerimeter() {
        return 2 * Math.PI * this.radious;
    }

    void setRadious(double NewRadious) {
        this.radious = NewRadious;
    }

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }
    
}
