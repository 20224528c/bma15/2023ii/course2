/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.uni.d.zerpa.horacio.operators;

import java.io.File;

/**
 *
 * @author Horacio Zerpa Diaz <horacio.zerpa.d@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.print("-");
        System.out.println("Operators!");
        System.out.println("+");
        
        int a=4;
        double c=3+2 * --a;
        System.out.println("c: "+c);
        
        //aritmetic operators
        int z= 2*5 + 3*4 - 8;
        System.out.println("z = "+z);
        
        int z1= 2*((5+3)*4 - 8);
        System.out.println("z1 = "+z1);
        
        System.out.println(9/3);
        System.out.println(10/3);
        int z2=1;
        long z3=33;
        System.out.println(z2*z3);
        System.out.println("bytes: "+Integer.SIZE/8);
        System.out.println("bytes: "+Long.SIZE/8);
        
        double x5=39.21;
        float y5=2.1f;
        System.out.println(x5+y5);
        
        short x6=10;
        short y6=3;
        System.out.println(x6/y6);
        
        short x7=17;
        float y7=3;
        double z7=38;
        System.out.println(x7*y7/z7);
        
        boolean x8=false;
        System.out.println("x8: "+x8);
        x8=!x8;
        System.out.println("x8: "+x8);
        
        double x9=1.21;
        System.out.println("x9: "+x9);
        x9=-x9;
        System.out.println("x9: "+x9);
        
        int counter=0;
        System.out.println("counter: "+counter);
        System.out.println("counter: "+ ++counter);
        System.out.println("counter: "+counter);
        System.out.println("counter: "+ counter--);
        System.out.println("counter: "+counter);
        
        int x=3;
        int y= ++x*5/x-- + --x;
        System.out.println("x: "+x);
        System.out.println("y: "+y);
        
        //int x=1.8;
        //short y=1912222;
        //int z=9f;
        //long t=19323088432;
        
        int  a1=(int)1.8;
        System.out.println("a1: "+a1);
        
        short y1=(short)1921222;
        System.out.println("y1: "+y1);
        
        int c1=(int)9f;
        System.out.println("c1: "+c1);
        
        long t=1827832183721732372L;
        System.out.println("t: "+t);
        
        short a2=10;
        short b2=3;
        short c2=(short)(a2*b2);
        System.out.println("c2: "+c2);
        
        int a3=2, b3=3;
        a3*=b3;
        System.out.println("a3: "+a3);
        
        long a4=18;
        int b4=5;
        //b4=b4*a4;
        b4*=a4;
        
        long a5=5;
        long b5=(a5=3);
        System.out.println("a5: "+a5);
        System.out.println("b5: "+b5);
        
        int a6=10, b6=20, c6=30;
        System.out.println(a6<b6);
        System.out.println(a6<=b6);
        System.out.println(a6>=b6);
        System.out.println(a6>c6);
        
        
        boolean a7=true||(y<4);
        System.out.println("a7: "+a7);
        
        Object o;
        o=new Object();
        if(o!=null&& o.hashCode()<5){
            //do something   
        }
        if(o!=null& o.hashCode()<5){
            //do something
        }
        
        int a8=6;
        boolean b8=(a8>=6)||(++a8<=7);
        System.out.println("a8: "+a8);
        System.out.println("b8: "+b8);
        
        boolean b9=false;
        boolean a9=(b9=true);
        System.out.println("a9:"+a9);
        System.out.println("b9: "+b9);
        
        System.out.println("Equality operator");
        File p1=new File("file.txt");
        File q1=new File("file.txt");
        File r1=p1;
        System.out.println("p1==q1? "+(p1==q1));
        System.out.println("p1==r1? "+(p1==r1));
    }
}
