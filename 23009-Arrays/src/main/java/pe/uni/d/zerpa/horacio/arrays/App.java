/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.uni.d.zerpa.horacio.arrays;

/**
 *
 * @author Horacio Zerpa<hzerpad@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Arrays!");
        int arraySize = 5;
        double[] styleJavaArray;
        double styleCppArray[];

        styleJavaArray = new double[arraySize];
        styleCppArray = new double[arraySize];

        styleJavaArray[0] = 5;
        styleJavaArray[1] = 4;
        styleJavaArray[2] = 3;
        styleJavaArray[3] = 2;
        styleJavaArray[4] = 1;

        System.out.println("styleJavaArray[2]: " + styleJavaArray[2]);
        System.out.println("");

        for (int i = 0; i < styleJavaArray.length; i++) {
            //System.out.println("i: " + i);
            System.out.println("styleJavaArray[" + i + "] = " + styleJavaArray[i]);
        }

        System.out.println("Definiendo");
        for (int i = 0; i < styleJavaArray.length; i++) {
            styleJavaArray[i] = i;
        }

        System.out.println("Visualización");
        for (int i = 0; i < styleJavaArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + styleJavaArray[i]);
        }

        //Inicializando forma 2
        double[] myArray = {1.9, 2.9, 3.4, 3.5, 7.2};
        System.out.println("Visualización");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + myArray[i]);
        }

        /*
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese " + myArray.length + " valores: ");
        for(int i = 0; i < myArray.length; i++){
            myArray[i] = input.nextDouble();
        }
        
        System.out.println("Visualización");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + myArray[i]);
        }
         */
        //Iniciando valores aleatorios
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = Math.random() * 100;
        }
        System.out.println("Visualización");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + myArray[i]);
        }

        double total = 0;
        for (int i = 0; i < myArray.length; i++) {
            total += myArray[i];
        }
        System.out.println("total: " + total);

        //Encontrar el elemneto más grande
        double max = myArray[0];
        int indexOfMax = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] > max) {
                max = myArray[i];
                indexOfMax = i;
            }
        }
        System.out.println("max: " + max);
        System.out.println("index of max: " + indexOfMax);

        //mezclar elementos
        for (int i = 0; i < myArray.length; i++) {
            //generar un index aleatorio
            int j = (int) (Math.random() * myArray.length);
            //System.out.println("j: " + j);
            double temp = myArray[i];
            myArray[i] = myArray[j];
            myArray[j] = temp;
        }

        System.out.println("Visualización");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + myArray[i]);
        }

        //Shifting
        double temp = myArray[0];
        for (int i = 1; i < myArray.length; i++) {
            myArray[i - 1] = myArray[i];
        }

        myArray[myArray.length - 1] = temp;
        System.out.println("Visualización");
        for (int i = 0; i < myArray.length; i++) {
            System.out.println("styleJavaArray[" + i + "] = " + myArray[i]);
        }
        
        //
        String [] months = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"};
        java.util.Scanner input = new java.util.Scanner(System.in);
        System.out.println("Ingrese un número del mes (1 al 12): ");
        int monthNumber = input.nextInt();
        System.out.println("El mes es: " + months[monthNumber - 1]);
    }
}
