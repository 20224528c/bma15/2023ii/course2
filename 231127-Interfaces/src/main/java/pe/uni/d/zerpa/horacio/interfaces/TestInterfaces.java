/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.uni.d.zerpa.horacio.interfaces;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public class TestInterfaces {

    public static void main(String[] args) {
        System.out.println("Test Interfaces!");
        Object[] objects = {new Orange(), new Apple(), new Chicken(), new Tigger()};
        for (Object object : objects) {
            if (object instanceof Animal) {
                System.out.println(((Animal) object).sound());
            }
            if(object instanceof Edible){
                System.out.println(((Edible) object).howtoEat());
            }
        }
    }
}
