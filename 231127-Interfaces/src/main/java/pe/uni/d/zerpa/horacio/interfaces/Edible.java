/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.uni.d.zerpa.horacio.interfaces;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public interface Edible {
    public abstract String howtoEat();
}
