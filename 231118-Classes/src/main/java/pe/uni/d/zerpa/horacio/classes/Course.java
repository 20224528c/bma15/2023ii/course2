/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.classes;

/**
 *
 * @author Horacio Zerpa Diaz <hzerpad@uni.pe>
 */
public class Course {
    private String courseName;
    private String[] students;
    private int numberOfstudents;
    private final int MAX_STUDENTS = 100;
    
    public Course(String courseName){
        this.courseName = courseName;
        students = new String[MAX_STUDENTS];
    }

    public String getCourseName() {
        return courseName;
    }

    public int getNumberOfstudents() {
        return numberOfstudents;
    }

    public String[] getStudents() {
        return students;
    }
    
    void addStudent(String student){
        students[numberOfstudents] = student;
        numberOfstudents++;
    }
    
    void dropStudents(String student){
        // 
    }
}
