/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.uni.d.zerpa.horacio.abstractclasses;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public class TestCircleRectangle {

    public static void main(String[] args) {
        System.out.println("TestCircleRectangle");
        
        Circle c1 = new Circle(5);
        Rectangle r1 = new Rectangle(5,5);
        System.out.println("Circulo : " + c1.toString());
        System.out.println("Area del circulo: " + c1.getArea());
        System.out.println("Perimetro del circulo: " + c1.getPerimeter());
        System.out.println("Rectangulo : " + r1.toString());
        System.out.println("Area del rectangulo: " + r1.getArea());
        System.out.println("Perimetro del rectangulo: " + r1.getPerimeter());
        
    }
}
