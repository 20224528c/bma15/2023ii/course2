/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.interfaces;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public class Tigger extends Animal {

    @Override
    public String sound() {
        return "Roar!!!";
    }

}
