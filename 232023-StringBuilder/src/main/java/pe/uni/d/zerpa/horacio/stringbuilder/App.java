/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package pe.uni.d.zerpa.horacio.stringbuilder;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("StringBuilder");
        String alpha = "";
        for (char character = 'a'; character <= 'z'; character++) {
            alpha += character;
        }
        System.out.println("alpha: " + alpha);
        
        // StringBuilder
        StringBuilder beta = new StringBuilder();
        for (char character = 'a'; character < 'z'; character++) {
            beta.append(character);
        }
        System.out.println("beta: " + beta);
        
        StringBuilder sb = new StringBuilder("star");
        sb.append("+middle");
        StringBuilder same = sb.append("+end");
        System.out.println("same: " + same);
        System.out.println("sb: " + sb);
        
        StringBuilder a = new StringBuilder("abc");
        StringBuilder b = a.append("de");
        b = b.append("f").append("g");
        System.out.println("a: " + a);
        System.out.println("b: " + b);
        
        // constructs
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("animal");
        StringBuilder sb3 = new StringBuilder(10);  // reservar un número de posiciones para caracteres
        
        // substring()
        StringBuilder sb4 = new StringBuilder("animals");
        String sub = sb4.substring(0, 5);
        System.out.println("sub: " + sub);
        System.out.println(sb4.indexOf("a"));
        System.out.println(sb4.indexOf("al"));
        sub = sb4.substring(sb4.indexOf("a"), sb4.indexOf("al"));
        System.out.println("sub: " + sub);
        System.out.println("length: " + sb4.length());
        char character = sb4.charAt(6);
        System.out.println("character: " + character);
        
        // insert
        StringBuilder sb5 = new StringBuilder("animals");
        sb5.insert(7, "-");
        sb5.insert(0, "-");
        sb5.insert(4, "-");
        System.out.println("sb5: " + sb5);
        
        // delete
        StringBuilder sb6 = new StringBuilder("abcdef");
        sb6.deleteCharAt(5);
        System.out.println("sb6: " + sb6);
        sb6.delete(1, 3);
        System.out.println("sb6: " + sb6);
        
        // reverse
        StringBuilder sb7  =new StringBuilder("ABC");
        sb7.reverse();
        System.out.println("sb7: " + sb7);
        String s = sb7.toString();
        System.out.println("s: " + s);
    }
}
