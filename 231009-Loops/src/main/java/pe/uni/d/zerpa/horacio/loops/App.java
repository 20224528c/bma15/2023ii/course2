/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.uni.d.zerpa.horacio.loops;

/**
 *
 * @author Horacio
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Loops");
        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ,");
        }

        System.out.println("");
        System.out.println("Loop 2");
        for (int i = 10; 0 < i; i--) {
            System.out.println("i: " + i);
        }

        /*
        System.out.println("Loop3");
        for (int i = 0;; i++) {
            System.out.println("i: " + i);
        }
         */
        for (int i = 0; i <= 5; i++) {
            for (int j = 0; j <= 3; j++) {
                System.out.println("(" + i + ";" + j + ")\t");
            }
            System.out.println("");
        }

        System.out.println("While loop");
        int i = 1;
        while (i <= 5) {
            System.out.println("i: " + i);
            i++;
        }

        System.out.println("While loop 2");
        int j = 5;
        while (1 <= j) {
            System.out.println("j: " + j);
            j--;
        }

        /*
        System.out.println("While loo 3");
        int k = 1;
        while (true) {
            System.out.println("k: " + k);
            k++;
        }
         */
        int a = 1;
        while (a <= 5) {
            int b = 1;
            while (b <= 3) {
                System.out.println("(" + a + ";" + b + ")");
                b++;
            }
            System.out.println("");
            a++;
        }

        System.out.println("Do while loop");
        int p = 1;
        do {
            int q = 1;
            do {
                System.out.print("(" + p + ";" + q + ")\t");
                q++;
            } while (q <= 3);
            System.out.println("");
            p++;
        } while (p <= 5);
    }
}
