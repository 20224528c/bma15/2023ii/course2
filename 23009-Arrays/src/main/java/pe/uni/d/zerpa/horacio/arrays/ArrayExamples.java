/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.arrays;

/**
 *
 * @author Horacio
 */
public class ArrayExamples {

    public static void main(String[] args) {
        new ArrayExamples().linealSearchOne();
        new ArrayExamples().linealSearchTwo();
        new ArrayExamples().orderArray();
    }

    private void orderArray() {
        System.out.println("-----------------------------");
        System.out.println("Ordenar un array de numero de manera ascendente");
        int[] numeros = {9, 5, 7, 3, 8, 6, 4, 0, 2, 1};
        for (int i = 0; i < numeros.length; i++) {

            int minIndex = i;
            int min = numeros[minIndex];
            for (int j = i + 1; j < numeros.length; j++) {
                if (min > numeros[j]) {
                    min = numeros[j];
                    minIndex = j;
                }
            }

            System.out.println("Mínimo: " + min + "Posicion: " + minIndex);

            numeros[minIndex] = numeros[i];
            numeros[i] = min;

            for (int j = 0; j < 10; j++) {
                System.out.print(numeros[j] + "");
            }
        }
        /*
        minIndex = 1;
        min = numeros[minIndex];
        for (int i = 1; i < numeros.length; i++) {
            if(min > numeros[i]){
                min = numeros[i];
                minIndex = i;
            }
        }
        
        System.out.println("Mínimo: " + min);
        System.out.println("Posición: " + minIndex);
        
        numeros[minIndex] = numeros[0];
        numeros[0] = min;
        
        for (int i = 0; i < numeros.length; i++) {
            System.out.print(numeros[i] + "");
        }
         */
    }

    private void linealSearchTwo() {
        System.out.println("------------------------------");
        System.out.println("Busqueda de los elmentos (keys encontrados en el array");
        int[] array = {1, 4, 8, 5, 2, 9, 4, 8};
        int key = 8;
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                System.out.println("Posicion encontra: " + i);
                found = true;
            }
        }

        if (!found) {
            System.out.println("Número no encontrado");
        }
    }

    private void linealSearchOne() {
        System.out.println("Busqueda lineal donde un elemento (key) donde todos los elementos del array son diferentes");
        int[] array = {1, 4, 8, 5, 2, 9, 3};
        int key = 3;

        int index = -1;
        boolean found = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                index = i;
                found = true;
                break;
            }
        }

        if (found) {
            System.out.println("Elemento encontrado esta en la posicion: " + index);
        } else {
            System.out.println("No se encontró el elemento");
        }
    }
}
