/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.uni.d.zerpa.horacio.interfaces;

/**
 *
 * @author Horacio Zerpa <hzerpad@uni.pe>
 */
public class Apple extends Fruit {
    
    @Override
    public String howtoEat() {
        return "Apple: make a Struddle";
    }
}
